<?php wp_footer(); ?>

<!-- ############# Rodapé ############### -->
<div id="newsletter">
    <div class="container">
        <div class="chamada">
            Cadastre-se e receba novidades
        </div>
        <div class="form">
           <?php echo do_shortcode('[contact-form-7 id="48" title="News"]'); ?>
        </div>
    </div>
</div>
<div id="map">
        <div id="gmap-holder">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.009277924309!2d-51.17791648488517!3d-30.036591681885188!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9519778968f0329f%3A0x8a97cf924996197!2sAv.+Carlos+Gomes%2C+1859+-+Auxiliadora%2C+Porto+Alegre+-+RS!5e0!3m2!1spt-BR!2sbr!4v1469826918472" width="100%" height="350" frameborder="0" style="border:0; pointer-events:none" allowfullscreen=""></iframe>
        </div>
    </div>

    <footer>
        <div class="container">
        <div class="coluna">
            <div class="info-kaizen">
                <div class="logo">
                    <img src="https://www.agenciakaizen.com.br/assets/images/xlogo-footer.png.pagespeed.ic.wXC1mtRdGi.webp" alt="">
                </div>
                <div class="descricao">
                    Com mais de 15 anos de experiência de mercado, a Agência Kaizen é uma empresa Partner do Google,
                    especializada em Marketing Digital de Alta Performance. Com escritórios espalhados por vários estados do país, 
                    possuímos um sistema de automação em geração de leads.
                </div>
                <div class="google-kaizen">
                    <img src="<?php echo get_template_directory_uri() .'/assets/img/google.png' ?>" alt="">
                </div>
            </div>
        </div>
        <div class="coluna">
            <div class="info-kaizen">
                <div class="heading">
                    <h2>Soluções</h2>
                </div>
            <?php wp_nav_menu(array('menu' => 'Solucoes', 'theme_location' => '__no_such_location',
'fallback_cb' => false )); ?>
            </div>
        </div>
        <div class="coluna">
            <div class="form-social">
                <?php echo do_shortcode('[contact-form-7 id="181" title="Rodape"]') ?>
            </div>
            <div class="social">
                <div>Ou acompanhe pelas redes</div>
            
            <ul>
                <li><i class="icon-facebook"></i></li>
                <li><i class="icon-google-plus"></i></li>
                <li><i class="icon-instagram"></i></li>
                <li><i class="icon-social-linkedin"></i></li>
                <li><i class="icon-social-youtube"></i></li>
            </ul>
            </div>
        </div>
        </div>
    </footer>
    <div id="copyright">
        <div class="container">
    <p>
        A <a href="https://www.agenciakaizen.com.br/agencia-digital" title="Agencia Digital">Agência Digital</a> Kaizen é especialista em Marketing Digital de Alta Performance com escritórios em varios locais do país, possuimos sistemas de automação em geração de <a href="https://www.agenciakaizen.com.br/leads" title="Leads">leads</a> e gestão de campanhas de publicidade online com foco em geração de resultados para negócios locais e e-commerce nas redes sociais, portais de conteúdo e mecanismos de procura. Conheça nossa Consultoria <a href="https://www.agenciakaizen.com.br/o-que-e-seo" title="O que SEO?">SEO</a> e nossa Gestão <a href="https://www.agenciakaizen.com.br/google-adwords" title="Google Adwords">Google Adwords</a>
        <img src="data:image/webp;base64,UklGRlYBAABXRUJQVlA4TEkBAAAvE4AEEH9jkG2korw/5t29w2yPgcK2bdtYJSfdatM2YPCa+gbGDXhAGB0KQgQAGPABQM8GHIiFAAhgdQBgQgSCAbOjAIQAwYYoPIACYJah9gM2AjDREMHqSIh+txAAAN22bePNjm2ndoO6qWOrVlCb+/3/3S8fniGi/wnF2OlTDeHjdRe0Kvzy668p3nYMULd5iC3KF06oDobWqWabcgLScvWk/8Lp0HMS6T+KRzNE6Jejn/Q9xQoWfpmB3pulxt88Zoc2pHO31Poehx2Oh94XR51HKp4CECV+Uw5WqfiMQMy88ZvSO9aoSMCqQ+yuR+kcccplGLpZCE+e0jZML5RaBlQoliB8qcUJI8QeRdeMAqWfSagybJh1Rco5SPHUfnXArM7WoHxjgeS6onidQ7RH+dIF2bBZ9+lWPzh62zVCrTt6/ufj2YYbMgA=" data-pagespeed-url-hash="3270771372" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
        <a href="https://www.agenciakaizen.com.br/sitemap.xml" title="Sitemap">&nbsp;Sitemap</a>
                        </p>
            </div>
    </div>
<!-- ############# Scripts ############### -->
<script>
    jQuery(function($){
        $(window).on('scroll', function() {
            scrollPosition = $(this).scrollTop();
            if (scrollPosition >= 80) {
                // If the function is only supposed to fire once
                $("#cabecalho").addClass('sticky');
                $(".topBar").slideUp();
                // Other function stuff here...
            }
            else if(scrollPosition <= 80 ) {
                $("#cabecalho").removeClass('sticky');
                $(".topBar").slideDown();
            }
        });
    })
</script>
<script>
jQuery(function($){

    // $(".ultimos-posts-blog").slick({
    // slidesToShow: 3,
    // slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 2500,
    // });

     $(".contatos").slick({
    slidesToShow: 1,
    variableWidth: true,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2500,
    });
});
</script>
<script>
	jQuery(function($){
		$( "#tabs" ).tabs({
            collapsible: true,
            active: 0
        });
        
        $("#tabs li.ui-tabs-active > a").click(false);
         
	} );
    </script>
<script>
jQuery(function($){  
$(document).ready(function() {
$(window).load(function() {
$('#pageloader').css('opacity', '0').delay(350).hide(0);
$('body').css('overflow', 'auto');
});
});
})
</script>

</body>
</html>
