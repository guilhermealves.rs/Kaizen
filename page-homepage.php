<?php
get_header(); ?>

    <!-- Sessão 1 - Home -->

    <div id="home">
        <?php get_template_part('template/sessao', 'home') ?>
    </div>

    <!-- Sessão 2 - Metodologia + Cases -->

    <div id="metodologia" class="sessao">
        <?php get_template_part('template/sessao', 'metodologia') ?>
    </div>

    <!-- Sessão 3 - Soluções Digitais -->  

    <div id="edicao-atual" class="sessao">
        <?php get_template_part('template/sessao', 'solucoes-digitais') ?>
    </div>

    <!-- Sessão 4 - Blog Digital -->  

    <div id="blog-digital" class="sessao blog">
        <?php get_template_part('template/sessao', 'blog-digital') ?>
    </div>

<?php
get_footer();