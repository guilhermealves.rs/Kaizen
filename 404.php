<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Cantinho_da_Val
 */

get_header(); ?>

<section class="error-404 not-found" style="margin:0;">
    <header class="page-header">
        <h1 class="page-title" style="color:#fff"><?php esc_html_e( 'Página não existente!' ); ?></h1>
    </header><!-- .page-header -->
</section>

<?php
get_footer();
