<?php
get_header(); ?>

    <!-- Sessão 1 - Edições -->

    <div id="edicoes">
        <?php get_template_part('template/sessao', 'edicoes') ?>
    </div>
    
    <div id="conteudo">
        <!-- Sessão 2 - Blog -->

        <div id="blog" class="sessao">
            <?php get_template_part('template/sessao', 'blog') ?>
        </div>

        <!-- Sessão 3 - Sidebar -->  

        <div id="sidebar" class="sessao">
            <?php get_template_part('template/sessao', 'sidebar') ?>
        </div>
    </div>
<?php
get_footer();