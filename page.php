<?php

get_header(); 

// Nome da página

$nome_page = esc_html( get_the_title() );
$nome_page = strtolower($nome_page);
$nome_page = str_replace(' ', '-', $nome_page);

$clientes = get_permalink( get_page_by_title( 'Clientes' ) );

// Imagem do cabeçalho 

$heading_page = get_field('heading_page');
if(!empty($heading_page)):
else:
$heading_page = 'https://www.agenciakaizen.com.br/assets/images/xbg-interna.jpg.pagespeed.ic.dmcR-BlYbh.webp';
endif;

// Sidebar 

$sidebar = get_field_object('status_da_sidebar');
$status_sidebar = $sidebar['value'];
if($status_sidebar && $status_sidebar == 'Ativada'):
	// Com a sidebar ativa, nenhuma classe é necessária para desativá-la.
else:
	// Diferentemente de quando desativada.
	$the_sidebar = ' sidebar-off';
endif;

?>

<div class="<?php global $post; echo $post->post_name;?> interna">
	<div class="heading-page" style="background:url('<?php echo $heading_page ?>'">
		<div class="info">
			<div class="titulo">
				<h1><?php the_title() ?> </h1>
			</div>
			<div class="meta">
				<!-- -->
				<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
					<a href="<?php echo home_url() ?>" itemprop="url">
					<span itemprop="title">Agência Kaizen</span>
				</a> &gt;
				</div>  
				<!-- -->
				<?php  if(is_single()) {?>
				<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="<?php echo home_url() . '/' . $cat[0]->slug ?>" itemprop="url">
					<span itemprop="title"><?php echo $cat[0]->name ?></span>
				</a> &gt;
				</div>
				<?php } ?>
				<!-- -->
				<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				<?php if ( get_post_type( get_the_ID() ) == 'cliente' ) { ?>
				<a href="<?php echo $clientes ?>" itemprop="url">
					<span itemprop="title">Clientes</span>
				<?php } else { ?> 
				<a href="<?php the_permalink() ?>" itemprop="url">
					<span itemprop="title"><?php the_title() ?></span>
				<?php } ?>
				</a>
				<!-- -->
				</div>  
			</div>
		</div>
	</div>

<div class="box-interna">
	<div class="container">
		<div class="pagina">
			<div class="conteudo<?php echo $the_sidebar ?>">
				<div class="inicio">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
				</div>
				<div class="final">
				<?php 
					if ( '' != locate_template( 'template/page/'.$nome_page.'.php' ) ) {
						include( get_stylesheet_directory() . '/template/page/'.$nome_page.'.php' );
					}
				?>
				</div>
			</div>

			<div class="sidebar<?php echo $the_sidebar ?>">
				<?php echo do_shortcode( '[contact-form-7 id="7" title="Especialista"]' ) // Formulário ?>
				<?php wp_nav_menu(  array ( 'menu' => 'Sidebar','walker' => new WPSE_78121_Sublevel_Walker ) );  // Menu Sidebar ?>
			</div>

			</div>
		
			<div class="conteudo-adicional">
				<?php 
					if($adicionais):
					foreach ($adicionais as $adicional) { ?>
					<div class="conteudo-<?php echo $adicional ?>" class="sessao">
						<?php get_template_part('template/sessao', $adicional) ?>
					</div>
					<?php }
					endif;
				?>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();