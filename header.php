<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<div class="item"_nk rel="profile" href="http://gmpg.org/xfn/1d"v>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script> -->
<link href="https://file.myfontastic.com/p4uUXPejECK4vhCgLyCmka/icons.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway:300,500,700,900" rel="stylesheet">
<?php wp_head(); ?>
<link rel="icon" href="<?php echo get_template_directory_uri();?>/img/favicon.png" type="image/x-icon" />
</head>

<body <?php body_class(); ?>>
<div id="pageloader">
    <div class="kaizen">
        <div class="img">
            <img src="https://www.agenciakaizen.com.br/assets/images/xlogo.png.pagespeed.ic.bbaJ-nJiUZ.webp" alt="">
            <div class="line"></div>
        </div>
    </div>
</div>
<?php
if(is_front_page()) {
echo '<div class="bodywrap">';
}
else {
    '<div class="bodywrap interno">';
}
?>

    <div id="wrap">
        
<header id="cabecalho" class="<?php if ( is_front_page() ) { echo ""; } else { echo 'interno'; } ?>">
<div class="topBar">
                <div class="atendimento">Atendimento Online</div>
                <div class="contatos">
                    <div class="item"_>PELOTAS 53 3199-1011</div>
                    <div class="item"_>FLORIANÓPOLIS 48 3052-9082</div>
                    <div class="item"_>SÃO PAULO 11 4858-8398</div>
                    <div class="item"_>CAXIAS DO SUL 54 3199-0007</div>
                    <div class="item"_>BLUMENAU 47 4054-9231</div>
                    <div class="item"_>CURITIBA 41 3891-0633</div>
                    <div class="item"_>PORTO ALEGRE 51 3191-2137</div>
                </div>
            </div>

    <div class="container">
        <div class="logo">
            <img src="https://www.agenciakaizen.com.br/assets/images/xlogo.png.pagespeed.ic.bbaJ-nJiUZ.webp" alt="">
        </div>
        <div class="box-menu">
            
            <nav>
            <?php wp_nav_menu(array('menu' => 'Principal', 'theme_location' => '__no_such_location',
'fallback_cb' => false )); ?>
            </nav>
        </div>
    </div>
</header>