<?php

/**
Desenvolvido com <3 por Agência CAQO 
**/

get_header(); ?>

<div class="<?php global $post; echo $post->post_name;?> interna">
    <header style="background: url('<?php echo the_post_thumbnail_url(); ?>')">
		
	</header>
	
	<div class="container">

		
		<hr>
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
    <?php if(function_exists('bcn_display'))
    {
        bcn_display();
    }?>
		</div>
	<div class="conteudo">
        <div class="col-md-9 equipamentos">
                <div class="col-md-12 lista-equipamentos">
                    <ul>
                        <?php $args = array( 'taxonomy' => 'category', 'orderby' => 'name','order'   => 'ASC' );
                        $cats = get_categories($args);
                        foreach($cats as $cat) { ?>
                        <li>
                            <a href="<?php echo get_home_url() . '/categoria/' . $cat->slug . '?post_type=equipamentos'; ?>"><?php echo $cat->name; ?></a>
                        </li>	
                        <?php
                                               }
                        ?>
                    </ul>
                    <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>    
                    <div class="col-md-4">
                        <a href="<?php echo the_permalink(); ?>">
                            <div class="card">
                                <div class="img" style="background:url('<?php echo the_post_thumbnail_url() ?>')">
                                </div>
                                <h1><?php the_title(); ?></h1>
                                <p class="descricao_curta">
                                    <?php $descricao = CFS()->get('preview-item');
                                    echo substr($descricao, 0, 150) . ' [...]'; ?>
                                </p>
                            </div>
                        </a>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <?php $especialidades = CFS()->get( 'especialidade' );
                if (!empty($especialidades)) { ?>
                <div class="sessao especialidade">
                    Produto indicado para: 
                    <span>
                        <?php
                    foreach ( $especialidades as $espcialidadeId ) {
                        $espcialidade_id = $espcialidadeId; 
                        $espcialidadepost = get_post($espcialidade_id); 
                        $espcialidadeslug = $espcialidadepost->post_title;
                        $espcialidadelink = get_permalink(); ?>
                        <a href="<?php echo $espcialidadelink; ?>"><?php echo $espcialidadeslug ?></a>
                        <?php
                    }
                } else { echo "";}
                        ?>
                    </span>
                </div>
                <div class="col-md-3 sidebar-produto">
				<h1>Conheça toda a linha de equipamentos:</h1>
					
				<div id="fade">
				<?php
					$post = get_post();
					$idCat = get_the_category();
					$cat_id = $idCat[0]->cat_ID;
					$loops = new WP_Query( array( 'post_type' => 'equipamentos', 'posts_per_page' => -1, 'cat' => $cat_id ) ); ?>
				<?php while ( $loops->have_posts() ) : $loops->the_post(); global $post;
				?>
				<a href="<?php echo the_permalink(); ?>">
					<img src="<?php echo the_post_thumbnail_url($post->ID) ?>" alt="">
				</a>
				<?php endwhile; ?>
				</div>
				<hr>
				<h2>Ficou interessado?</h2>
				<div class="contatos"><a href="">Entre em contato</a>
				
				<p>+55 (24) 2453-5416</p>

				<p>+55 (24) 2453-5354</p>

				<p>+55 (24) 2453-5364</p>
				<a href="">Envie um email</a>
								

				</div>
			</div>

            </div>

        </div>
    </div>
</div>

    <?php
    get_footer();