<?php

get_header(); 

// Nome da página

$nome_page = esc_html( get_the_title() );
$nome_page = strtolower($nome_page);
$nome_page = str_replace(' ', '-', $nome_page);

// Clientes

$clientes = get_permalink( get_page_by_title( 'Clientes' ) );

// Imagem do cabeçalho 

$heading_page = get_field('heading_page');
if(!empty($heading_page)):
else:
$heading_page = 'https://www.agenciakaizen.com.br/assets/images/xbg-interna.jpg.pagespeed.ic.dmcR-BlYbh.webp';
endif;

// Sidebar 

$sidebar = get_field_object('status_da_sidebar');
$status_sidebar = $sidebar['value'];
if($status_sidebar && $status_sidebar == 'Ativada'):
	// Com a sidebar ativa, nenhuma classe é necessária para desativá-la.
else:
	// Diferentemente de quando desativada.
	$the_sidebar = ' sidebar-off';
endif;

?>