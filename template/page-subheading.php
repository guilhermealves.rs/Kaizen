<div class="heading-page" style="background:url('<?php echo $heading_page ?>'">
		<div class="info">
			<div class="titulo">
				<h1><?php the_title() ?> </h1>
			</div>
			<div class="meta">
			<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="<?php echo home_url() ?>" itemprop="url">
				<span itemprop="title">Agência Kaizen</span>
			</a> &gt;
			</div>  
			<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			<?php if ( get_post_type( get_the_ID() ) == 'cliente' ) { ?>
			<a href="<?php echo $clientes ?>" itemprop="url">
				<span itemprop="title">Clientes</span>
			<?php } else { ?> 
			<a href="<?php the_permalink() ?>" itemprop="url">
				<span itemprop="title"><?php the_title() ?></span>
			<?php } ?>
			</a>
			&gt;
			</div>  
			<!-- <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			<a href="http://www.example.com/clothes/dresses/real/green" itemprop="url">
				<span itemprop="title">Real Green Dresses</span>
			</a>
			</div> -->
			</div>
		</div>
	</div>