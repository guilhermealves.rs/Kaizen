<!-- ############# Soluções ############### -->
<div id="wrap-solucoes">
	<div class="heading">
		<h2><a href="">METODOLOGIA KAIZEN</a></h2>
		<div class="sub-line"></div>
	</div>
	<div class="solucoes">
		<div class="item rastreador-de-midias">
			<img src="https://www.agenciakaizen.com.br/assets/icons/svg/phone-call.svg" alt="">
			<h2>rastreador de mídias</h2>
		</div>
		<div class="item landing-page">
			<img src="https://www.agenciakaizen.com.br/assets/icons/svg/web-development.svg" alt="">
			<h2>landing page</h2>
		</div>
		<div class="item inbound-marketing">
			<img src="https://www.agenciakaizen.com.br/assets/icons/svg/attraction.svg" alt="">
			<h2>inbound marketing</h2>
		</div>
		<div class="item plataforma-de-ecommerce">
			<img src="https://www.agenciakaizen.com.br/assets/icons/svg/online-shop.svg" alt="">
			<h2>plataforma de ecommerce</h2>
		</div>
		<div class="item google-adwords">
			<img src="<?php echo get_template_directory_uri() . '/assets/img/social.svg' ?>" alt="">
			<h2>google adwords</h2>
		</div>
		<div class="item desenvolvimento-de-sites">
			<img src="https://www.agenciakaizen.com.br/assets/icons/svg/development.svg" alt="">
			<h2>desenvolvimento de sites</h2>
		</div>
		<div class="item otimizacao-de-sites">
			<img src="https://www.agenciakaizen.com.br/assets/icons/svg/seo.svg" alt="">
			<h2>otimização de sites</h2>
		</div>
		<div class="item producao-de-videos-para-internet">
			<img src="https://www.agenciakaizen.com.br/assets/icons/svg/video.svg" alt="">
			<h2>produção de videos para internet</h2>
		</div>
		<div class="item facebook-lead-ads">
			<img src="https://www.agenciakaizen.com.br/assets/icons/svg/facebook.svg" alt="">
			<h2>facebook lead ads</h2>
		</div>
		<div class="item anuncio-no-waze">
			<img src="https://www.agenciakaizen.com.br/assets/icons/svg/waze.svg" alt="">
			<h2>anúncio no waze</h2>
		</div>
		<div class="item geracao-de-leads">
			<img src="https://www.agenciakaizen.com.br/assets/icons/svg/certo.svg" alt="">
			<h2>geração de leads</h2>
		</div>
		<div class="item email-marketing">
			<img src="https://www.agenciakaizen.com.br/assets/icons/svg/mail.svg" alt="">
			<h2>email marketing</h2>
		</div>
		<div class="item marketing-nas-redes-socias">
			<img src="https://www.agenciakaizen.com.br/assets/icons/svg/social.svg" alt="">
			<h2>marketing nas redes socias</h2>
		</div>
		<div class="item servicos">
			<img src="https://www.agenciakaizen.com.br/assets/icons/svg/plus.svg" alt="">
			<h2>serviços</h2>
		</div>
	</div>
</div>











