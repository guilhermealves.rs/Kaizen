<div id="blog-digital">
    <div class="ultimos-posts-blog">
        <!-- Inicio lista de posts -->

        <?php $loops = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => -1 ) ); ?>
        <?php while ( $loops->have_posts() ) : $loops->the_post(); global $post; ?>
        <?php 
        $autor = get_the_author_meta('ID');
            ?>
        <div class="item">
            <div class="box-post">
                <div class="heading">
                    <div class="autor">
                        <div class="img">
                            <?php echo get_avatar( get_the_author_meta( 'ID' ) , $autor );?>
                        </div>
                        <div class="info">
                            <div class="nome"><?php the_author_meta('display_name') ?></div>
                            <div class="data"><?php echo get_the_date('Y/m/d \a\t g:i') ?></div>
                        </div>
                    </div>
                </div>
                <div class="conteudo-post">
                    <h2 class="titulo" title="Políticas de Anúncios do Google AdWords" ><?php echo the_title() ?></h2>
                    <div class="descricao">
                        <?php echo excerpt(15); ?>
                    </div>
                </div>
                <div class="btn-leia-mais"><a href="<?php the_permalink() ?>">Leia Mais...</a></div>
            </div>
        </div>
        <?php endwhile; ?>
        
        <!-- Fim lista de posts -->
    </div>
</div>