
<div class="formulario-contato">
    
    <div id="tabs">
        <ul>
            <li clas="ui-tabs-active"><a href="#contato">Contato</a></li>
            <li><a href="#trabalhe-conosco">Trabalhe conosco</a></li>
        </ul>
        <div id="contato">
            <div class="box-form">
                <?php echo do_shortcode('[contact-form-7 id="172" title="Contato"]'); ?>
            </div>
        </div>
        <div id="trabalhe-conosco">
            <div class="box-form">
                <?php echo do_shortcode('[contact-form-7 id="173" title="Trabalhe Conosco"]'); ?>
            </div>
        </div>

    </div>

</div>

<div class="enderecos-contato">

    <?php

    if( have_rows('enderecos') ):

        while ( have_rows('enderecos') ) : the_row();
        echo '<div class="endereco"> ';
            echo '<h3>' . get_sub_field('cidade') . '</h3>';
            echo '<h3>' . get_sub_field('telefone') . '</h3>';
            echo '<p>' . get_sub_field('rua') . '</p>';
        echo '</div> ';

        endwhile;

    else :

    endif;

    ?>

</div>