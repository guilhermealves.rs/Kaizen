<div class="lst-clientes">
<?php $loops = new WP_Query( array( 'post_type' => 'cliente', 'posts_per_page' => -1 ) ); ?>
<?php while ( $loops->have_posts() ) : $loops->the_post(); global $post;?>

<div class="item">
    <div class="box">
        <a href="<?php the_permalink() ?>">
            <img src="<?php echo get_the_post_thumbnail_url()?>" alt="">
        </a>
    </div>
</div>
<?php endwhile; ?>
</div>

<?php $adicionais = array('solucoes-digitais');?>