<!-- ############# Metodologia ############### -->
<div id="wrap-metodologia">
    <div class="container">

        <div class="metodologia-kaizen">
            <div class="heading">
                <h2><a href="">METODOLOGIA KAIZEN</a></h2>
                <div class="sub-line"></div>
            </div>
            <div class="conteudo-metodologia">
                <div class="item atrair">
                    <img src="https://www.agenciakaizen.com.br/assets/images/xmeth01.png.pagespeed.ic.90NQoSQ0zW.webp" alt="">
                    <h2>Atrair</h2>
                </div>
                <div class="item converter">
                    <img src="https://www.agenciakaizen.com.br/assets/images/xmeth02.png.pagespeed.ic.PDyOeciiKG.webp" alt="">
                    <h2>Converter</h2>
                </div>
                <div class="item relacionar">
                    <img src="https://www.agenciakaizen.com.br/assets/images/xmeth03.png.pagespeed.ic.QYzOziF9DD.webp" alt="">
                    <h2>Relacionar</h2>
                </div>
                <div class="item aprimorar">
                    <img src="https://www.agenciakaizen.com.br/assets/images/xmeth04.png.pagespeed.ic.oAlc24x5QL.webp" alt="">
                    <h2>Aprimorar</h2>
                </div>
            </div>
            <div class="btn-saiba-mais">Saiba Mais</div>
        </div>

        <div class="cases-kaizen">
            <div class="heading">
                <h2><a href="">CASES DA KAIZEN AGÊNCIA DIGITAL</a></h2>
                <div class="sub-line"></div>
            </div>
            <img src="https://www.agenciakaizen.com.br/assets/images/xcase.png.pagespeed.ic.55djqmQsIB.webp" alt="">
        </div>
    </div>
</div>