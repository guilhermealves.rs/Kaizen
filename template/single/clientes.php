<?php

get_header(); 

// Imagem do cabeçalho 

$heading_page = get_field('heading_page');
if(!empty($heading_page)):
else:
$heading_page = 'https://www.agenciakaizen.com.br/assets/images/xbg-interna.jpg.pagespeed.ic.dmcR-BlYbh.webp';
endif;

// Sidebar 

$sidebar = get_field_object('status_da_sidebar');
$status_sidebar = $sidebar['value'];
if($status_sidebar && $status_sidebar == 'Ativada'):
	// Com a sidebar ativa, nenhuma classe é necessária para desativá-la.
else:
	// Diferentemente de quando desativada.
	$the_sidebar = ' sidebar-off';
endif;

?>

<div class="<?php global $post; echo $post->post_name;?> interna">
	<div class="heading-page" style="background:url('<?php echo $heading_page ?>'">
		<div class="info">
			<div class="titulo">
				<h1><?php the_title() ?> </h1>
			</div>
			<div class="meta">
				<div class="autor">
					<?php echo get_the_author() ?>
				</div>
				<div class="categoria">
					<?php echo get_the_category() ?>
				</div>
			</div>
			
		</div>
	</div>
<div class="box-interna">
	<div class="container">
		<div class="conteudo<?php echo $the_sidebar ?>">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
			<?php 
			if(is_page('clientes')):
				get_template_part('template/interna', 'clientes');
			endif;
			?>
		</div>
		<div class="sidebar<?php echo $the_sidebar ?>">
			<?php echo do_shortcode( '[contact-form-7 id="7" title="Especialista"]' ) // Formulário ?>
			<?php wp_nav_menu(  array ( 'menu' => 'Sidebar','walker' => new WPSE_78121_Sublevel_Walker ) );  // Menu Sidebar ?>
		</div>
		</div>
	</div>
</div>

<?php
get_footer();